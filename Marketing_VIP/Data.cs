﻿/*
 *  Created by : Eugene OMalley
 *  Created : 07/01/2015
 *  Purpose : the allow VIP marketing to quickly find replacement waves for VIP sales rep 
 *          : 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Newtonsoft.Json.Linq;

namespace Marketing_VIP
{
    public class Data
    {
         // initialize variables need to data tier off appliccation
        private string prod_conn = System.Configuration.ConfigurationManager.ConnectionStrings["db_connection_string_prod"].ConnectionString;
        private string my_query;
        private DataTable tcs_data = new DataTable();
        private string distance = "0";
        private string time = "0";
        private string requesturl = "http://maps.googleapis.com/maps/api/directions/json?origin=";
        private string content;

        // get the distance between the member sales is at and the select destination
        public string getDistance(string origin, string destination)
        {
            // have the application sleep for approximatly one second to allow to analize last data pulled form google maps
            System.Threading.Thread.Sleep(1000);
            requesturl += origin + "&destination=" + destination + "&sensor=false";
            content = fileGetContents(requesturl);
            //pasrse string in a object called o
            JObject directions = JObject.Parse(content);
            try
            {
                distance = (string)directions.SelectToken("routes[directions].legs[directions].distance.value");
                time = (string)directions.SelectToken("routes[directions].legs[directions].duration.text");
                return distance + ',' + time;
            }
            catch
            {
                //  return distance + ',' + time;
            }
            return distance + ',' + time;
            //ResultingDistance.Text = distance;
        }

        protected string fileGetContents(string fileName)
        {
            string sContents = string.Empty;
            string me = string.Empty;
            try
            {
                if (fileName.ToLower().IndexOf("http:") > -1)
                {
                    System.Net.WebClient wc = new System.Net.WebClient();
                    byte[] response = wc.DownloadData(fileName);
                    sContents = System.Text.Encoding.ASCII.GetString(response);

                }
                else
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(fileName);
                    sContents = sr.ReadToEnd();
                    sr.Close();
                }
            }
            // If there is an return the error uable to connect to server for user
            // Needs login in backend DB
            catch { sContents = "unable to connect to server "; }
            
            // return the string with reponse data from googles maps
            return sContents;
        }

        public DataTable cleints(string acct)
        { 
            //clear data table
            tcs_data.Clear();
            Oracle.ManagedDataAccess.Client.OracleConnection my_conn = new Oracle.ManagedDataAccess.Client.OracleConnection(prod_conn);
            try
            {
                // DB SQL
                my_query = @"select m.acct_num,m.Phone_1,m.Phone_2 from resorts.member m join resorts.member_type mt on mt.MEMBER_TYPE_ID=m.MEMBER_TYPE_ID
                                where zip = ( select zip from resorts.member where acct_num = :acct)
                                and m.acct_num not like 'V%'
                                                           and (m.VIP_PRES_GIVEN < add_months(sysdate,(-36)) or m.VIP_PRES_GIVEN is null)
                                                           and m.VIP_OPT_OUT='N'
                                and M.Acct_Num <> :acct 
                                                           and mt.UP_TYPE like 'ACTIVE%'";
                //Open connection
                my_conn.Open();
                // crreate oracle command
                Oracle.ManagedDataAccess.Client.OracleCommand my_comm = new Oracle.ManagedDataAccess.Client.OracleCommand(my_query, my_conn);

                my_comm.Parameters.Add(new OracleParameter("@acct", acct));
                //added this 1-12-14
                //create oracle data adapter
                Oracle.ManagedDataAccess.Client.OracleDataAdapter my_adt = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(my_comm);
                my_conn.Close();
                // fill data table
                my_adt.Fill(tcs_data);
                //retrun data table
                return tcs_data; ;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            finally
            {
                //close connection no matter what 
                my_conn.Close();
            }

        }
        public DataTable get_next(string acct,Int32 startDis, Int32 endDis)
        {

            tcs_data.Clear();

            try
            {

                Oracle.ManagedDataAccess.Client.OracleConnection my_conn = new Oracle.ManagedDataAccess.Client.OracleConnection(prod_conn);
            my_query = @"select m.acct_num
                               ,M.Phone_1
                               ,M.Phone_2 
                         from resorts.member m join resorts.member_type mt on mt.MEMBER_TYPE_ID=m.MEMBER_TYPE_ID
                         where m.acct_num not like 'V%'
                            and (m.VIP_PRES_GIVEN < add_months(sysdate,(-36)) or m.VIP_PRES_GIVEN is null)
                            and m.VIP_OPT_OUT='N'
                            and mt.UP_TYPE like 'ACTIVE%'
                            and m.zip IN(select distinct z.zip
                                         from RESORTS.Zip_Code z
                                            , RESORTS.Zip_Code z2
                                         where z2.zip = (select zip from resorts.member where Acct_Num = :member )
                                        and (NVL(3963,0) * 
                                             ACOS((sin(NVL(Z.Latitude,0) / 57.29577951) * SIN(NVL(Z2.Latitude,0) / 57.29577951)) +
                                            round(COS(NVL(Z.Latitude,0) / 57.29577951) * COS(NVL(Z2.Latitude,0) / 57.29577951) * COS(NVL(Z2.Longitude,0) / 57.29577951 - NVL(Z.Longitude,0)/ 57.29577951),10 )
                                        ))between :startingMiles and :endMiles)";
            
             my_conn.Open();
             Oracle.ManagedDataAccess.Client.OracleCommand my_comm = new Oracle.ManagedDataAccess.Client.OracleCommand(my_query, my_conn);
            
             my_comm.Parameters.Add(new OracleParameter(":member", acct));
             my_comm.Parameters.Add(new OracleParameter(":startingMiles", startDis));
             my_comm.Parameters.Add(new OracleParameter(":endMiles", endDis));
                //added this 1-12-14
             Oracle.ManagedDataAccess.Client.OracleDataAdapter my_adt = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(my_comm);
                 my_conn.Close();
                my_adt.Fill(tcs_data);

                return tcs_data;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            
        }

        public void store_error(string error_message, string acctNumber , string address)
        {
            Oracle.ManagedDataAccess.Client.OracleConnection my_conn = new Oracle.ManagedDataAccess.Client.OracleConnection(prod_conn);
            try
            {

            // The DB SQL
                my_query = @"Insert into Log.VIP_errors (Acct_num , Address , Error)
                    values (:acctNumber , :address , :Error) ";

                // open connection
                my_conn.Open();
                // create oracle command
                Oracle.ManagedDataAccess.Client.OracleCommand my_comm = new Oracle.ManagedDataAccess.Client.OracleCommand(my_query, my_conn);
                //add parmaeters to SQL
                my_comm.Parameters.Add(new OracleParameter("@acctNumber", acctNumber));
                my_comm.Parameters.Add(new OracleParameter("@address", address));
                my_comm.Parameters.Add(new OracleParameter("@Error", error_message));
            }
            catch (Exception e)
            {
                    throw new Exception(e.Message);
            }
            finally
            {

            }
        }

    }
}